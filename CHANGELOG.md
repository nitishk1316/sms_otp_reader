## [0.0.1] - 26-10-20

* Initial release.

## [0.0.2] - 26-10-20

* Added LICENSE.

## [0.0.3] - 26-10-20

* Fixed plugins issue

## [0.0.4] - 26-10-20

* Fixed Package issue